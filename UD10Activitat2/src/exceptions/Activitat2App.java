package exceptions;
import exceptions.excepciones;

public class Activitat2App {

	public static void main(String[] args) {
        int num;
        
        try {
        	//Aqui le doy un valor al 12 para probar
            num = 11;
            //Miro que mensaje de error tengo que dar
            if (num == 12) {
                throw new excepciones(num);
            } else {
                throw new excepciones(num);
            }
        } catch (excepciones ej) {
            System.out.println(ej.getMessage());
        }
 
    }

}
